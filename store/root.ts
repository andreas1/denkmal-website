import { Module, ActionContext } from 'vuex'
import { AllState } from 'store'
import throttle from 'lodash/throttle'

export { RootModule, RootState }

interface RootState {
  isNavigationVisible: boolean
  swiperIndex: number
  dataLoadedAt?: number
}

let RootModule: Module<RootState, AllState> = {
  namespaced: true,
  state: (): RootState => ({
    isNavigationVisible: false,
    swiperIndex: 0,
    dataLoadedAt: undefined,
  }),
  getters: {},
  mutations: {
    toggleNavigationVisibility(state: RootState, visible?: boolean) {
      if (visible === undefined) {
        visible = !state.isNavigationVisible
      }
      state.isNavigationVisible = visible
    },
    setDataLoaded(state: RootState) {
      state.dataLoadedAt = currentTimestamp()
    },
  },
  actions: {
    async initStore({ state, dispatch }) {
      // Don't load data twice (server + client)
      if (!state.dataLoadedAt) {
        await dispatch('loadData')
      } else {
        // Don't `await`, because we already have data, just not the newest
        void dispatch('loadDataIfOld')
      }
      await dispatch('datetime/startInterval')
    },
    async initStoreAfterMount({ dispatch }) {
      await dispatch('useragent/updateUseragent')
    },

    async loadDataIfOld({ state, dispatch }) {
      let loadAgainAfter = 60
      let loadAgain = state.dataLoadedAt === undefined || state.dataLoadedAt + loadAgainAfter < currentTimestamp()
      if (loadAgain) {
        await dispatch('loadDataThrottled')
      }
    },
    async loadData({ dispatch, commit }) {
      await Promise.all([dispatch('regions/loadRegions'), dispatch('events/loadEventsByWeek')])
      commit('setDataLoaded')
    },
    loadDataThrottled: throttle(async ({ dispatch }: ActionContext<RootState, AllState>) => {
      await dispatch('loadData')
    }, 60 * 1000),

    setSwiperIndex({ state }, index: number) {
      state.swiperIndex = index
    },
  },
}

function currentTimestamp(): number {
  return Math.floor(new Date().getTime() / 1000)
}
