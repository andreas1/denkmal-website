import css from './assets/css'
import { generateDynamicRoutes } from './assets/dynamicRoutes'
import { Configuration } from '@nuxt/types'
import { getTransformer as getGraphqlTransformer } from 'ts-transform-graphql-tag'
import { DenkmalConfig } from './assets/denkmalConfig'

let denkmalConfig: DenkmalConfig = {
  DENKMAL_DEBUG: process.env.DENKMAL_DEBUG || '',
  DENKMAL_WEBSITE_URL: process.env.DENKMAL_WEBSITE_URL || 'http://localhost:3000',
  DENKMAL_WEBSITE_VERSION: process.env.DENKMAL_WEBSITE_VERSION || 'dev',
  DENKMAL_API_URL: process.env.DENKMAL_API_URL || 'https://api.denkmal.org/graphql',
  DENKMAL_MOCK_NOW: process.env.DENKMAL_MOCK_NOW || '',
  GOOGLE_MAPS_API_KEY: process.env.GOOGLE_MAPS_API_KEY || 'AIzaSyC5s-73wOdKqfCzNXV6YNJ-RNgJOl6HL80',
  GOOGLE_ANALYTICS_IDS: process.env.GOOGLE_ANALYTICS_IDS || 'UA-570541-8,UA-570541-9',
  SENTRY_DSN: process.env.SENTRY_DSN || '',
}

let denkmalDebug = !!denkmalConfig.DENKMAL_DEBUG

let nuxtConfig: Configuration = {
  modern: 'client',
  env: denkmalConfig,

  // See https://nuxtjs.org/api/configuration-head/
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Denkmal.org Website',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // See https://pwa.nuxtjs.org/modules/meta.html#options
  meta: {
    name: 'Denkmal.org',
    theme_color: css.meta,
    ogHost: denkmalConfig.DENKMAL_WEBSITE_URL,
    ogImage: { path: '/og-image.png' },
  },

  loading: { color: css.meta },

  // See https://pwa.nuxtjs.org/modules/manifest.html
  manifest: {
    name: 'Denkmal.org',
    short_name: 'Denkmal',
    description: 'Denkmal.org',
    start_url: denkmalConfig.DENKMAL_WEBSITE_URL,
    display: 'standalone',
  },

  css: ['@/assets/styles/main.scss'],

  modules: [
    '@nuxtjs/pwa',
    [
      'nuxt-i18n-module',
      {
        defaultLanguage: 'de',
        languages: ['de', 'fr', 'it', 'en'],
        rootUrl: denkmalConfig.DENKMAL_WEBSITE_URL,
      },
    ],
    '@nuxtjs/sentry',
  ],

  sentry: {
    dsn: denkmalConfig.SENTRY_DSN,
    clientIntegrations: {
      Dedupe: {},
    },
    config: {
      environment: 'production',
      release: `denkmal-website@${denkmalConfig.DENKMAL_WEBSITE_VERSION}`,
      beforeSend: (event: any, hint: any) => {
        const exception = hint.originalException
        const message = exception.message || ''
        const patterns = {
          'webpack-loadingchunk': /Loading chunk \d+ failed/,
          'api-transport': /API transport error/,
        }
        const pattern = Object.entries(patterns).find((f) => f[1].test(message))
        if (pattern) {
          event.fingerprint = [pattern[0]]
        }
        return event
      },
    },
  },

  plugins: [
    // Order of plugins is important!
    // - 'denkmal-api' before 'vuex-sync', before the Store is using the API
    // - 'vuex-sync' before 'vuex-init', to restore state before updating/initializing it
    { src: '~/plugins/denkmal-api' },
    { src: '~/plugins/vuex-sync' },
    { src: '~/plugins/vuex-init' },
    { src: '~/plugins/google-analytics', mode: 'client' },
    { src: '~/plugins/vue-ripple', mode: 'client' },
    { src: '~/plugins/vue-touch-events', mode: 'client' },
  ],

  generate: {
    fallback: '404.html',
    concurrency: 50,
    routes: () => generateDynamicRoutes(denkmalConfig),
  },

  router: {
    extendRoutes(routes: any, _resolve: any) {
      routes = routes.map((route: any) => {
        route.path = route.path
          .replace(/:region/, ':region([\\w\\däöü%]{4,})')
          .replace(/:date/, ':date(\\d{4}-\\d{2}-\\d{2})')
          .replace(/:event/, ':event([\\d\\w-]{10,})')
        return route
      })
    },
  },

  dev: denkmalDebug,
  debug: denkmalDebug,
  vue: {
    config: {
      silent: false,
      devtools: denkmalDebug,
      performance: denkmalDebug,
      errorHandler: denkmalDebug
        ? (e: Error) => {
            throw e
          }
        : (e: Error) => {
            console.warn(e)
          },
      warnHandler: (e: Error) => {
        console.warn(e)
      },
    },
  },

  buildModules: ['@nuxt/typescript-build'],
  build: {
    cache: true,

    extend(config: any, { loaders: { imgUrl, fontUrl } }: any) {
      if (denkmalDebug) {
        config.devtool = 'source-map'
      }

      // Inline images < 8kb
      imgUrl.limit = 8000
      // Inline all fonts (< 50kb)
      fontUrl.limit = 50000
    },

    transpile: ['swiper', 'dom7', 'vuejs-datepicker'],

    html: {
      minify: {
        // Not minifying HTML makes `nuxt generate` much faster,
        // and the resulting HTML more readable.
        collapseBooleanAttributes: false,
        decodeEntities: false,
        minifyCSS: false,
        minifyJS: false,
        processConditionalComments: false,
        removeEmptyAttributes: false,
        removeRedundantAttributes: false,
        trimCustomFragments: false,
        useShortDoctype: false,
      },
    },

    optimization: {
      minimize: !denkmalDebug,
    },
  },
  loaders: {
    ts: {
      // Compile GraphQL tagged template strings
      // See https://github.com/firede/ts-transform-graphql-tag
      getCustomTransformers: () => ({ before: [getGraphqlTransformer()] }),
    },
  },
}

export default nuxtConfig
