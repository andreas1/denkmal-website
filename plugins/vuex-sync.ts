import storage from 'local-storage-fallback'
import { Store } from 'vuex'

export default ({ store }: any) => {
  if (process.client) {
    let window_any = window as any
    window_any.onNuxtReady(async () => {
      // Sync "userPreferences" with `localStorage`
      let sync = new VuexSync(storage, 'vuex', store, ['userPreferences'])
      sync.restoreState()
      sync.subscribeState()

      // Load "selectedRegionSlug"
      await store.dispatch('regions/loadSelectedRegionFromStorage')
    })
  }
}

class VuexSync<S> {
  private storage: StorageFallback
  private storageKey: string
  private store: Store<S>
  private modules: string[]

  constructor(storage: StorageFallback, storageKey: string, store: Store<S>, modules: string[]) {
    this.storage = storage
    this.storageKey = storageKey
    this.store = store
    this.modules = modules
  }

  restoreState() {
    let stateSaved = this.filterData(this.readData())
    let state = this.store.state as StoredData
    for (let module in stateSaved) {
      if (state.hasOwnProperty(module)) {
        state[module] = { ...state[module], ...stateSaved[module] }
      }
    }
    this.store.replaceState(state as S)
  }

  subscribeState() {
    this.store.subscribe((mutation, state: any) => {
      let mutationModule = getModuleFromMutation(mutation.type)
      if (!mutationModule) {
        return
      }
      if (!this.modules.includes(mutationModule)) {
        return
      }
      let stateToSave = this.filterData(state)
      setTimeout(() => {
        this.writeData(stateToSave)
      }, 0)
    })
  }

  private filterData(data: StoredData): StoredData {
    let dataFiltered: StoredData = {}
    for (let module of this.modules) {
      if (data.hasOwnProperty(module)) {
        dataFiltered[module] = data[module]
      }
    }
    return dataFiltered
  }

  private readData(): StoredData {
    let data = this.storage.getItem(this.storageKey)
    if (typeof data !== 'string') {
      return {}
    }
    try {
      return JSON.parse(data)
    } catch (e) {
      return {}
    }
  }

  private writeData(data: StoredData) {
    let str = JSON.stringify(data)
    this.storage.setItem(this.storageKey, str)
  }
}

type StoredData = {
  [module: string]: any
}

function getModuleFromMutation(mutationName: string): string | undefined {
  let parts = mutationName.split('/')
  if (parts.length < 2) {
    return undefined
  }
  return parts[0]
}
