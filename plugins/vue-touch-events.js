import Vue from 'vue'
import Vue2TouchEvents from 'vue2-touch-events'

export default (context) => {
  Vue.use(Vue2TouchEvents, {
    tapTolerance: 0,
    swipeTolerance: 0,
  })
}
