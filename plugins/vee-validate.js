import Vue from 'vue'
import VeeValidate from 'vee-validate'
import en from 'vee-validate/dist/locale/en'
import de from 'vee-validate/dist/locale/de'
import fr from 'vee-validate/dist/locale/fr'

export const initValidation = () =>
  Vue.use(VeeValidate, {
    dictionary: {
      en: en,
      de: de,
      fr: fr,
    },
  })
