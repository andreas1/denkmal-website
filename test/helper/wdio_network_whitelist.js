class NetworkWhitelist {
  constructor(config) {
    this.allowedUrls = config.allowedUrls
    this.allowedUrlsOriginal = config.allowedUrls
    this.requestInterceptionInstalled = false
  }

  _resetWhitelist() {
    this.allowedUrls = this.allowedUrlsOriginal
  }

  _isUrlAllowed(url) {
    return this.allowedUrls.some((urlWhitelist) => {
      return url.startsWith(urlWhitelist)
    })
  }

  _installRequestInterceptor() {
    global.browser.cdp('Network', 'setRequestInterception', {
      patterns: [{ urlPattern: '*' }],
    })

    global.browser.on('Network.requestIntercepted', async (event) => {
      let url = event.request.url
      let requestAllowed = this._isUrlAllowed(url)
      if (!requestAllowed) {
        console.warn(`Aborting not allowed request to URL: '${url}'`)
      }

      global.browser.cdp('Network', 'continueInterceptedRequest', {
        interceptionId: event.interceptionId,
        errorReason: requestAllowed ? undefined : 'Aborted',
      })
    })
  }

  before(capabilities, specs) {
    global.browser.addCommand('allowUrl', (url) => {
      this.allowedUrls.push(url)
    })
  }

  beforeTest(test) {
    this._resetWhitelist()

    if (!this.requestInterceptionInstalled) {
      this._installRequestInterceptor()
      this.requestInterceptionInstalled = true
    }
  }
}

module.exports = NetworkWhitelist
