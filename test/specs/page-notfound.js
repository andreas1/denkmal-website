import { getDenkmalApiUrl } from '../helper/helpers'

describe('Unknown page URL', () => {
  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url('/does-not-exist')
  })

  it('shows error message', () => {
    $('.page-error').waitForDisplayed()
    expect($('body').getText()).toContain('This page could not be found')
  })
})
