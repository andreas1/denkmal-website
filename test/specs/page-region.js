import 'jasmine-expect'
import { todayDate, getDenkmalApiUrl } from '../helper/helpers'

describe('Region page', () => {
  const today = todayDate().toFormat('yyyy-MM-dd')

  beforeAll(() => {
    browser.url('/de/basel')
    $('.page-region-date').waitForDisplayed()
  })

  it('redirects to the events list', () => {
    expect(browser.getUrl()).toEndWith(`/de/basel/${today}`)
  })
})

describe('Region page with invalid region', () => {
  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url(`/de/miami`)
  })

  it('has an error message', () => {
    $('.page-error').waitForDisplayed()
    expect($('body').getText()).toContain("Invalid region: 'miami'")
  })
})
