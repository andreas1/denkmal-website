import 'jasmine-expect'

import { getEvent, todayDate, getDenkmalApiUrl } from '../helper/helpers'
import { DateTime } from 'luxon'

describe('Event page from next Friday', () => {
  let today = todayDate()
  let weekday = today.weekday
  let daysUntilFriday = weekday <= 5 ? 5 - weekday : 5 - weekday + 7
  let date = today.plus({ days: daysUntilFriday })
  let event = getEvent('basel', (event) => {
    return event.date === date.toFormat('yyyy-MM-dd')
  })

  beforeAll(() => {
    browser.url(`/de/basel/${event.date}/${event.eventId}`)
    $('.page-region-date-event').waitForDisplayed()
  })

  it('has event infos', () => {
    expect($('.EventDetailsMain .details-description').getText()).toMatch(/\w{2,}/)
  })

  it('has event description in title', () => {
    expect(browser.getTitle()).toEqual(`${$('.EventDetailsMain .details-description').getText()} - Denkmal.org`)
  })

  it('navigates back to current day in week overview', () => {
    $('.EventDetails .EventDetails-back').click()
    $('.page-region-date').waitForDisplayed()
    expect(browser.getUrl()).toEndWith(`/de/basel/${event.date}`)
  })
})

describe('Event page outside current week', () => {
  let today = todayDate()
  let event = getEvent('basel', (event) => {
    return DateTime.fromISO(event.date) < today
  })

  beforeAll(() => {
    browser.url(`/de/basel/${event.date}/${event.eventId}`)
    $('.page-region-date-event').waitForDisplayed()
  })

  it('navigates back to current week overview', () => {
    $('.EventDetails .EventDetails-back').click()
    $('.page-region-date').waitForDisplayed()
    expect(browser.getUrl()).toEndWith(`/de/basel/${today.toFormat('yyyy-MM-dd')}`)
  })
})

describe('Event page with unknown ID', () => {
  let event = getEvent('basel')

  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url(`/de/basel/${event.date}/11111111-1111-1111-1111-111111111111`)
  })

  it('has an error message', () => {
    $('.page-error').waitForDisplayed()
    expect($('body').getText()).toContain('Cannot find event')
  })
})

describe('Event page with wrong date in URL', () => {
  let event = getEvent('basel')

  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url(`/de/basel/2000-01-01/${event.eventId}`)
  })

  it('redirects to correct page', () => {
    $('.page-region-date-event').waitForDisplayed()
    expect(browser.getUrl()).toEndWith(`/de/basel/${event.date}/${event.eventId}`)
  })
})

describe('Event page with wrong region in URL', () => {
  let event = getEvent('basel')

  beforeAll(() => {
    browser.allowUrl(getDenkmalApiUrl())
    browser.url(`/de/tsri/${event.date}/${event.eventId}`)
  })

  it('redirects to correct page', () => {
    $('.page-region-date-event').waitForDisplayed()
    expect(browser.getUrl()).toEndWith(`/de/basel/${event.date}/${event.eventId}`)
  })
})
