describe('Regions page', () => {
  beforeAll(() => {
    browser.url('/de/regions')
    $('.page-regions').waitForDisplayed()
  })

  it('has correct title information', () => {
    expect(browser.getTitle()).toEqual('Wähle deine Stadt - Denkmal.org')
  })
})
