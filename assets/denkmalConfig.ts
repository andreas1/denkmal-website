type DenkmalConfig = {
  DENKMAL_DEBUG: string
  DENKMAL_WEBSITE_URL: string
  DENKMAL_WEBSITE_VERSION: string
  DENKMAL_API_URL: string
  DENKMAL_MOCK_NOW: string
  GOOGLE_MAPS_API_KEY: string
  GOOGLE_ANALYTICS_IDS: string
  SENTRY_DSN: string
}

export { DenkmalConfig }
